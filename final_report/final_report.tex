%% This is file `elsarticle-template-1-num.tex',
%%
%% Copyright 2009 Elsevier Ltd
%%
%% This file is part of the 'Elsarticle Bundle'.
%% ---------------------------------------------
%%
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%%
%% Template article for Elsevier's document class `elsarticle'
%% with numbered style bibliographic references
%%
%% $Id: elsarticle-template-1-num.tex 149 2009-10-08 05:01:15Z rishi $
%% $URL: http://lenova.river-valley.com/svn/elsbst/trunk/elsarticle-template-1-num.tex $
%%
\documentclass[preprint, 12pt]{elsarticle}

%% Use the option review to obtain double line spacing
%% \documentclass[preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
%% \documentclass[final,1p,times]{elsarticle}
%% \documentclass[final,1p,times,twocolumn]{elsarticle}
%% \documentclass[final,3p,times]{elsarticle}
%% \documentclass[final,3p,times,twocolumn]{elsarticle}
%% \documentclass[final,5p,times]{elsarticle}
%% \documentclass[final,5p,times,twocolumn]{elsarticle}

%% The graphicx package provides the includegraphics command.
\usepackage{graphicx}
%% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
%% The amsthm package provides extended theorem environments
%% \usepackage{amsthm}

%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers after \end{frontmatter}.
\usepackage{lineno}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage{multirow}
\newcolumntype{Y}{>{\centering\arraybackslash}X}


%% natbib.sty is loaded by default. However, natbib options can be
%% provided with \biboptions{...} command. Following options are
%% valid:

%%   round  -  round parentheses are used (default)
%%   square -  square brackets are used   [option]
%%   curly  -  curly braces are used      {option}
%%   angle  -  angle brackets are used    <option>
%%   semicolon  -  multiple citations separated by semi-colon
%%   colon  - same as semicolon, an earlier confusion
%%   comma  -  separated by comma
%%   numbers-  selects numerical citations
%%   super  -  numerical citations as superscripts
%%   sort   -  sorts multiple citations according to order in ref. list
%%   sort&compress   -  like sort, but also compresses numerical citations
%%   compress - compresses without sorting
%%
%% \biboptions{comma,round}

% \biboptions{}

%\journal{Journal Name}

\makeatletter
\def\ps@pprintTitle{%
 \let\@oddhead\@empty
 \let\@evenhead\@empty
 \def\@oddfoot{}%
 \let\@evenfoot\@oddfoot}
\makeatother

\usepackage{csquotes}
%\usepackage{pspicture}

\begin{document}

\begin{frontmatter}

%% Title, authors and addresses

\title{Applying the Rational Speech Acts Framework to Shallow Discourse Parsing}

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for the associated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for the associated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for the associated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%%
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}


%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{<author name>}
%% \address[label1]{<address>}
%% \address[label2]{<address>}

\author{Tim Patzelt, Till Ili\'{c}}

\address{Universit\"{a}t Potsdam, 31.03.2018}



\end{frontmatter}

%%
%% Start line numbering here if you want
%%
%%\linenumbers

%% main text

\section{Introduction}
\label{S:1}
Shallow Discourse Parsing is the task of identifying individual discourse relations present in a text. One part of this process is finding the discourse connectives (DCs) binding two arguments together. These connectives can be explicit such as in \textit{He went out \textbf{although} it was raining} or non-explicit as in \textit{It was late. He went to bed.}
Identifying the form (explicit or non-explicit) and the sense (temporal, causal etc.) of a given DC is a non-trivial task. Recent work has been done on adapting the Rational Speech Acts framework (modeling communication between two agents) for the tasks of discourse sense disambiguation \cite{yung2016interpretation} and modeling the usage of discourse connectives \cite{yung2016usage}.
We reimplemented parts of this work and experimented with different parameters. We analyzed different context measures for discourse sense classification but could not achieve any improvements compared to neglecting the context.
While trying to predict whether a speaker will use as implicit over an explicit DC, we were not able to reach the accuracy achieved by Yung et al.  \cite{yung2016usage}. Furthermore, by applying their model we did not beat our baseline which might be contributed to the size of the test set.


\section{Theoretical Background}
\label{S:2}

\subsection{Rational Speech Acts}
The  Rational Speech Acts (RSA) model is a formal framework representing the communication between a speaker and a listener as a process between two agents \cite{frank2012}.
The two agents (speaker and listener) consider each others discourse interpretations during the communication process and are described by two different models.
%The \textit{literal listener} interprets an utterance literally, whereas the \textit{pragmatic listener} and the \textit{pragmatic speaker} reason about the intentions of the other agent up to a certain level.
An in-depth explanation of the general RSA framework as originally formulated by Frank and Goodman can be found in \cite{frank2012}. The following sections focus on adapted models for the tasks of discourse relation interpretation and choice of markedness. 

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{rsa_model}
\caption{The RSA model for discourse interpretation, reprinted from \cite{yung2016interpretation}}
\label{lab:RSA}
\end{figure}

\subsubsection{RSA Applied to Discourse Relation Interpretation}
Yung, Duh, Komura and Matsumoto use the RSA framework to model the interpretation of discourse connectives \cite{yung2016interpretation}. 
They differentiate between the literal listener $L_{0}$ who interprets a discourse connective directly and the pragmatic listener $L_ {n}$ who emulates the language processing of the speaker for up to $n$ levels. A schematic representation of the model is shown in figure \ref{lab:RSA}. 
The listener chooses the sense $s$ that maximizes the probability $P_{L}(s \mid d, C)$.
The literal listener $L_{0}$ estimates the probability of a sense $s$ being the intended sense of the speaker using discourse connective $d$ under a context $C$ as follows:
$$P_{L_{0}}(s \mid d, C)=\frac{count(s,d,C)}{count(d,C)}$$
where the counts are extracted from an annotated corpus.
The pragmatic listener $L_{n}$ emulates the language processing of the speaker.
S/he estimates the probability of a sense $s$ being the intended sense of the speaker using $d$ with the following formula:
$$ P_{L_{n}}(s\mid d, C) = \frac{P_{S_{n}}(d\mid s, C) * salience(s)}{\sum_{s' \epsilon S} P_{S_{n}}(d\mid s', C) * salience(s')}$$
where the $salience$ of a sense $s$ describes the private preference of the listener. It is estimated as the frequency of the sense in the corpus:
$$salience(s) = \frac{count(s)}{\sum_{s'\epsilon S}{count(s')}}$$
Yung et al. define the probability of a pragmatic speaker $S_{n}$ to use connective $d$ for  sense $s$ as 
$$P_{S_{n}}(d\mid s, C)=\frac{exp(ln(P_{L_{n−1}}(s\mid d,C)) - cost(d))}{\sum_{d' \epsilon D} exp(ln(P_{L_{n-1}}(s \mid d', C)) - cost(d'))}$$
where $D$ is the set of all occurring connectives (including the empty string for non-explicit connectives). 
The aim of defining a \textit{cost} function for the connectives is to capture the production effort involved in retrieving the connective.
The cost of the non-explicit connective is therefore set to 0 as no word needs to be retrieved. Since most connectives are short the cost of all explicit connectives is set to the same constant, tuned during model training.
Yung et al. define the context $C$ as the immediately previous discourse relation (sense and form).
In their experiments they do mostly not find any improvement in accuracy using the context in this way over simply omitting it.
We have therefore chosen the evaluation of various new context measures as one of the main focus points of our work.

\subsubsection{RSA Applied to Choice of Markedness}
Yung, Duh, Komura and Matsumoto investigate on the reasons why a speaker marks a discourse relation either implicitly or explicitly. Using the RSA framework, they establish a speaker/listener relation \cite{yung2016usage}. This is done in order to predict whether a speaker will produce an explicit marker or not. As initial setting the speaker in the model knows the two arguments he wants to connect and he knows which discourse relation sense he wants to use. Given that, the model calculates a measure of informativeness and the cost of production. These both determine whether an explicit DC is used.
According to the Uniform Information Density (UID) principle \cite{levy2006}, communication can be seen as the transmission of information through a channel and a continuous flow of information is attempted to reach by a speaker. In a discourse relation there are two sources of information. One is the information in the arguments ($Arg1$ and $Arg2$), and the other is the information provided by the discourse connective given by the informativeness of  the discourse connective or $ Arg1$ and$ Arg2$, respectively. 
Given that the speaker knows which sense$ s$ he wants to use and in which context $C$ he will produce the discourse relation, the probability for producing utterance $ w$ can be given by:
$$ P(w|s,C) = \frac{exp(U(w;s,c))}{\sum_{w' \epsilon W}^{ }exp(U(w';s,C))} $$
The utterance $w$ comes from the set of all grammatically valid explicit discourse connectives and null as connective for an implicit relation. Yung et al. assume that implicit connectives are chosen when the arguments are very informative, such that no explicit connective is needed. On the other hand, when the arguments are not very informative the speaker uses an explicit connective to add information to the discourse. In a formal definition, the speaker produces an explicit DC if 
$$ exp(U(exp;s,C) > exp(U(null;s,C) + exp(U(args;s,C) $$
 and a implicit DC otherwise.
The utility $U$ of a discourse connective is the difference betweens its informativeness I and its production cost D.
$$ U(w;s,C) = I(s;w,C) - D(w) $$
The informativeness of a discourse connective $I(s;w,C)$ can be described by the negative suprisal. 
$$ I(s;w,C) = ln P(s|w,C) - ln P(s|C) $$
The context C is determined by the surrounding discourse relation in the range from 1 to 2 and for the production cost $D(w)$ different cost function are compared. These functions are: mean DC length, DC/arg2 ratio, prime frequency, prime distance and distance from start. If the DC is the implicit null connective, then the production cost is set to 0. The same holds for the arguments, because they are produced by the speaker anyway.
Furthermore, the informativeness of the arguments also plays a role if one wants to follow the UID principle and it can be calculated by the confidence of the implicit parser by Wang and Lan \cite{wang2015refined} predicting sense $s$  or the entropy of all candidate senses $s$:
$$ I(s;args,C) = w_a * ln P_p(s_{output}) $$
$$ I(s;args,C) = w_a * \sum_{s_p \epsilon O}^{} P_p(s_p) log P_p(s_p) $$
$O$ is the set of senses available to the parser and $ w_a $ is a parameter tuned on the dev set.

\section{Experiments}

\subsection{Data}

For our experiments we used data from the Penn Discourse Treebank (PDTB \cite{prasad2008}) as provided for the 2016 CoNLL shared task \cite{xue2016}.
The PDTB corpus contains texts from the Wall Street Journal, annotated manually with information regarding discourse structure.
Five different forms of relations and 42 distinct sense labels are distinguished in the PDTB.
All connectives are labeled with their corresponding forms and sense labels. Implicit connectives are additionally labeled with an explicit connective that captures the intended meaning of the implicit connective, selected by the annotator.

We kept the division of the provided data into training-, dev-, and test-set as provided for the CoNLL shared task of 2016, which means that our training data comes from sections 2-21 of the PDTB 2.0 release, the development set contains section 22 and the test set is made up of section 23 of the PDTB.
Our training set consists of 32535 samples, 17813 of which mark non-explicit relations.
The test set contains 1939 samples with 1016 of them marking non-explicit relations.

For the Sense Disambiguation task, we mapped the senses following the description of the CoNLL shared task \cite{xue2015} to a coarser set of 15 senses and we reduced the number of forms by categorizing each sample as either explicit or non-explicit as it was done by Yung et al.

For the Markedness task, some stronger restrictions had to be made on the training, development and test set. This is because all discourse relations which cannot be made implicit are excluded as the model should predict if the relation in question is marked by an explicit DC or not. We removed intra-sentential samples as they are only annotated explicitly in the PDTB 2.0, samples which do not have the form $Implicit$ or $Explicit$ and samples which do not have the ordering of $Arg1 - DC - Arg2$. Moreover, the arguments need to be adjacent as defined by the Implicit Parser and the DC must appear in at least 5\% of the cases for the respective sense. Due to the restriction that all sample discourse relations could be valid implicit ones, the test set shrinked to 1234 relations, the training set to 23428 relations and the dev set to 1006 relations.



\subsection{Sense Disambiguation}
We reimplemented the RSA model for discourse sense disambiguation and compared the accuracies on explicit and non-explicit relations using different context measures.
The cost of explicit DCs was set to 0.3 after experiments on the development set.
Yung et al. defined the context as the immediately previous discourse relation.
They expected some patterns to occur more often than others, but their empirical tests could not support their hypothesis.\\
Chenlo and Losoda compiled an overview of sentence features used in sentiment analysis tasks \cite{chenlo2014}. We evaluated whether such sentence features could improve the sense disambiguation by defining the context measure accordingly.
We analyzed positional features, length features and occurrences of syntactic patterns.
The results of the first experiments are shown in table \ref{tab:results_sense}.

\begin{table}
\begin{tabularx}{\columnwidth}{l Y Y Y Y}
    \toprule
    \multirow{2}{*}{\bfseries Context} & 
    \multicolumn{2}{c}{\bfseries Explicit} & \multicolumn{2}{c}{\bfseries Non-Explicit} \\\cmidrule(lr){2-5} 
     &  $L_{0}$ & $L_{1}$ & $L_{0}$ & $L_{1}$\\\cmidrule(lr){2-5}
     Constant&87.87&88.62&21.36&21.36\\
     Relative Position &88.62&88.62&21.36&21.36\\
     Argument Lengths (tertiles) & 87.54 & 80.93& 21.56 & 21.36 \\
     Argument Lengths 2 & 87.64 & 88.19 & 21.36 & 21.36 \\
     Syntactic Patterns & 87.11 & 87.54 & 21.26&21.36 \\
    \bottomrule
\end{tabularx}
\caption{Accuracies of sense classification on the test set using different contexts}
\label{tab:results_sense}
\end{table}

\subsubsection{Length features}
We hypothesized that the length of the arguments in a discourse relation could help determining the sense of the relation.
Intuitively it seemed that the length of the time specification in a sentence like 
\enquote{He did not work since \textit{time specification/reason specification}}
would be shorter (on average) than a reason specification in the same sentence.
Due to the limited size of the training set we could not just use the length of the arguments (in words or characters) directly, as this would have lead to a high dimensionality.
We therefore calculated the tertiles of the argument lengths in the training set and classified each argument as being either short, long or of a medium length.
We also tried just looking at which of the two arguments in a relation is longer.
Neither of the approaches yielded an improvement in the achieved accuracies. 
We believe that more training data might improve the accuracy of the first approach.

\subsubsection{Relative Positions}
We analyzed whether the relative position of the arguments inside their source text can help in sense disambiguation. For both of the arguments we looked whether they were located in the first or second half of the source document, trying again not to let the dimensionality grow too much. 

\subsubsection{Syntactic Patterns}
Turney compiled a list of five syntactic patterns of Part of speech tags that he used in the task of opinion mining reviews \cite{turney2002}.
It has been shown that adjectives are good indicators of subjective, evaluative sentences \cite{hatzi2000}.
We evaluated whether the (non-)existence of the five syntactic patterns in the arguments of a relation can be a useful context

\subsubsection{Results}

The evaluated context measures could not provide any improvement on the achieved accuracies.
One reason might be the size of the training data which does not provide many examples for each sense of each connective under all the contexts.
It could also be that the context does not play a significant role in the sense distribution of discourse markers and that the RSA model for discourse relation interpretation should therefore be simplified by neglecting the context.

\subsection{Choice of Markedness}
We applied the Rational Speech Acts model and the Uniform Information Density principle to the Choice of Markedness on the relations in our data set. As baseline we iterate over all relations in the dev and test set and choose $ w  \in W = \{(exp)licit,(imp)licit\} $ according to $ argmax_w P(s|w) $. The sense $ s $ is known beforehand, because the speaker first decides on the sense he wants to convey and then decides on the markedness.
After that, we calculated the probabilites $ P(s|w,C) $ and $P(s|C)$ from training set and added the different contexts $C$ to each relation as given by Yung et al.. The context information considers the sense and form of the surrounding relations in the range from 1 to 2.  Likewise to Yung et al., we deployed different cost functions for producing explicit DCs. The exact specification of context $C$ and the cost function $D(exp)$ can be seen in \cite{yung2016usage}. Subsequently, we passed the arguments of each discourse relation to the Implicit Sense Parser by Wang and Lan and recorded the probability distribution of the senses for each relation \cite{wang2015refined}. 

The next step is to set the tuning parameters $ w_a$ and $w_c$ such that the accuracy on the dev set is maximized. $w_a$ scales the informativeness of the arguments and therefore a higher value pushes the model to assign an implicit DC more easily. The second parameter $w_c$ weights the cost for producing an explicit DC. As the values for the cost function depend strongly on the choice of the cost function, $w_c$ serves to adjust $D(exp)$. After testing the model with values for $w_a,w_c \in [0.001,0.01,0.1,...,10,20,50]$ on the dev set, $w_a$ was set to 0.2 and $w_c$ to 50.

\subsubsection{Results}
The results for all the combinations of different settings are given in table \ref{tab:markedness_results}. As one can see highest accuracy was achieved with the baseline approach on both, the dev and the test set. Yung et al. report a baseline accuracy of about 0.85 on the dev and test set. This was not achieved by our experiment. The main reason for that could be that they used a different test and dev set. Our dev set consists of PDTB Section 22 and the test set of PDTB Section 23, while their respective sets consists of PDTB Section 0-1 and Section 23-24. The chose the splitting used by  Patterson and Kehler to compare their results \cite{patterson2013}. 
Our respected result was that the markedness model at least slightly improves all prediction and we could reproduce the results of Yung et al. In fact, we could not achieve their baseline accuracy and furthermore, the accuracy dropped for all tested configurations. Using different contexts $C$ as well as using different cost functions $D(exp)$ did not improve the accuracy at all. Besides the baseline, the best results were achieved by using the Top-level sense and form of the following discourse relation as context, the suprisal of the confidence of the Implicit Sense Parser into the predicted sense as informativeness of the arguments and the prime frequency as cost function. 
The fact, that our baseline accuracy is already more than 10 \% lower than the one from Yung et al. suggests that different scores are due to the different dev/test sets. We checked our code and especially the baseline approach, but could not find any differences to the method given by Yung et al..

\begin{table}
\centering
\begin{tabular}{lllllll}
 & context $C$ & arg. info. & cost function & \begin{tabular}[c]{@{}l@{}}dev: Sections 22\\ accuracy\end{tabular} & \begin{tabular}[c]{@{}l@{}}test: Section 23\\ accuracy\end{tabular} &  \\
\toprule
\textbf{BL} & constant & 0 & 0 & \textbf{0.716}  & \textbf{0.731} &  \\
\hline
 & F10 & 0 & 0 & 0.616 & 0.631 &  \\
 & SF10 & 0 & 0 &0.616  & 0.631 &  \\
 & F20 & 0 & 0 & 0.616 &  0.631&  \\
 & F11 & 0 & 0 &0.616  & 0.631 &  \\
 & TS10 & 0 & 0 &0.616  & 0.631 &  \\
\hline
 & constant & suprisal & 0 & 0.674 & 0.652 &  \\
 & constant & entropy & 0 & 0.537 & 0.548 &  \\
\hline
 & constant & 0 & mean DC length &0.522  & 0.538 &  \\
 & constant & 0 & DC/arg2 ratio &0.522   & 0.538 &  \\
 & constant & 0 & prime frequency & 0.522  &0.538  &  \\
 & constant & 0 & prime distance & 0.522  & 0.538 &  \\
 & constant & 0 & distance from start &0.522   &0.538  &  \\
\hline
 & F10 & entropy & DC/arg2 ratio &0.519  & 0.533 &  \\
 & TSF01 & suprisal & prime frequency & 0.698 &  0.654&  \\
 & TS01 & entropy & prime distance & 0.529 & 0.527 & 
\end{tabular}
\caption{Accuracy of predicted markedness: Implicit vs Explicit}
\label{tab:markedness_results}
\end{table}

\section{Conclusion}

We reimplemented the methods reported by Yung, Duh, Komura and Matsumoto \cite{yung2016interpretation}, \cite{yung2016usage}. The incorparation of the Rational Speech Act framework into the Sense Disambiguation and Markedness task seemed promising and reasonable. We were able to reproduce their results on the Sense Disambiguation task. Additionally, we tested different context measures but came to the result that neglecting the context achieves the best scores. As reported by Yung et al., the pragmatic listener $L1$ outperforms the literal listener $L0$ most of the times. 

Our results in the Markedness task were overall worse. We could not achieve the baseline accuracy and using the RSA and UID model did not improve the accuracy either. The data sets we tested the model on do not exactly resemble the ones used by Yung et al., but nevertheless is the model supposed to beat the baseline. 

All in all, the Rational Speech Act framework provides a reasoning which is suitable for shallow discourse parsing. Establishing a speaker/listener relations can help to understand the intended meaning of a discourse relation. It can also be applied to text generation in a similiar way, but further investigations should be conducted.


%% The Appendices part is started with the command \appendix;
%% appendix sections are then done as normal sections
%% \appendix

%% \section{}
%% \label{}

%% References
%%
%% Following citation commands can be used in the body text:
%% Usage of \cite is as follows:
%%   \cite{key}          ==>>  [#]
%%   \cite[chap. 2]{key} ==>>  [#, chap. 2]
%%   \citet{key}         ==>>  Author [#]

%% References with bibTeX database:

\bibliographystyle{model1-num-names}
\bibliography{bibo}

%% Authors are advised to submit their bibtex database files. They are
%% requested to list a bibtex style file in the manuscript if they do
%% not want to use model1-num-names.bst.

%% References without bibTeX database:

% \begin{thebibliography}{00}

%% \bibitem must have the following form:
%%   \bibitem{key}...
%%

% \bibitem{}

% \end{thebibliography}


\end{document}

%%
%% End of file `elsarticle-template-1-num.tex'.