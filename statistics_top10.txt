sense_cnt: 

('Expansion.Conjunction', 8379)
('Comparison.Contrast', 5292)
('Contingency.Cause.Reason', 3576)
('Expansion.Restatement', 2922)
('Contingency.Cause.Result', 2187)
('Temporal.Synchrony', 1662)
('Expansion.Instantiation', 1506)
('Temporal.Asynchronous.Precedence', 1354)
('Comparison.Concession', 1335)
('Contingency.Condition', 1304)



conn_cnt: 

('and', 3731)
('but', 3593)
('because', 2345)
('also', 1956)
('while', 1237)
('however', 1061)
('specifically', 1049)
('as', 1005)
('so', 964)
('if', 961)



conn_sense_cnt: 

(('and', 'Expansion.Conjunction'), 3649)
(('but', 'Comparison.Contrast'), 2623)
(('because', 'Contingency.Cause.Reason'), 2349)
(('also', 'Expansion.Conjunction'), 1954)
(('specifically', 'Expansion.Restatement'), 1019)
(('if', 'Contingency.Condition'), 953)
(('so', 'Contingency.Cause.Result'), 953)
(('for example', 'Expansion.Instantiation'), 816)
(('however', 'Comparison.Contrast'), 803)
(('then', 'Temporal.Asynchronous.Precedence'), 625)



exp_imp_cnt: 

('Explicit', 16216)
('Implicit', 14336)



exp_conn_cnt: 

(('Explicit', 'but'), 2924)
(('Explicit', 'and'), 2566)
(('Explicit', 'also'), 1537)
(('Explicit', 'if'), 961)
(('Explicit', 'when'), 851)
(('Explicit', 'while'), 685)
(('Explicit', 'because'), 684)
(('Explicit', 'as'), 626)
(('Explicit', 'after'), 434)
(('Explicit', 'however'), 432)



imp_conn_cnt: 

(('Implicit', 'because'), 1661)
(('Implicit', 'and'), 1165)
(('Implicit', 'specifically'), 1040)
(('Implicit', 'in fact'), 753)
(('Implicit', 'so'), 730)
(('Implicit', 'for example'), 711)
(('Implicit', 'but'), 669)
(('Implicit', 'however'), 629)
(('Implicit', 'in particular'), 569)
(('Implicit', 'while'), 552)



